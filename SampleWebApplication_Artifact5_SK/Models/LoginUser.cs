namespace SampleWebApplication_Artifact5_SK.Models
{
    public class LoginUserRequest
    {
        public String email { get; set; }

        public String password { get; set; }
    }

   
    public class LoginUserResponse
    {
        public String token { get; set; }

    }
}
