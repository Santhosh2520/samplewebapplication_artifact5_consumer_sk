using Newtonsoft.Json;
using SampleWebApplication_Artifact5_SK.CustomClient;
using SampleWebApplication_Artifact5_SK.Models;

namespace SampleWebApplication_Artifact5_SK.Services
{
    public class UserService : IUserService
    {
        private readonly ICustomHttpClient _customHttpClient;

        public UserService()
        {
            _customHttpClient = new CustomHttpClient("https://reqres.in");
        }

        public UserService(ICustomHttpClient customHttpClient)
        {
            _customHttpClient = customHttpClient;
        }

        public async Task<UserDetails> GetUserDetails(int id)
        {
            var apiPath = $"/api/users/{id}";

            try
            {
                var response = await _customHttpClient.GetAsync(apiPath);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var userDetails = JsonConvert.DeserializeObject<UserDetails>(json);

                    return userDetails;
                }
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            return null;
        }

        public async Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest)
        {
            var apiPath = $"/api/users";

            try
            {
                var response = await _customHttpClient.PostJsonAsync(apiPath, createUserRequest);

                if (response.StatusCode != System.Net.HttpStatusCode.Created)
                {
                    return null;
                }

                var json = await response.Content.ReadAsStringAsync();
                var createdUser = JsonConvert.DeserializeObject<CreateUserResponse>(json);

                return createdUser;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            return null;
        }


        public async Task<LoginUserResponse> LoginUser(LoginUserRequest loginUserRequest)
        {
            var apiPath = $"/api/login";

            try
            {
                var response = await _customHttpClient.PostJsonAsync(apiPath, loginUserRequest);

                if (response.StatusCode != System.Net.HttpStatusCode.OK)
                {
                    return null;
                }

                var json = await response.Content.ReadAsStringAsync();
                var loggedUser = JsonConvert.DeserializeObject<LoginUserResponse>(json);

                return loggedUser;
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine("\nException Caught");
                Console.WriteLine("Message :{0} ", e.Message);
            }

            return null;
        }






    }
}