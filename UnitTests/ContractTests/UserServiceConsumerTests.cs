using Xunit;
using PactNet;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using SampleWebApplication_Artifact5_SK.CustomClient;
using SampleWebApplication_Artifact5_SK.Services;
using SampleWebApplication_Artifact5_SK.Models;
using Match = PactNet.Matchers.Match;
using UnitTests.ContractTests.ConsumerPactConfiguration;



namespace UnitTests.ContractTests
{
    public class UserServiceContractTests
    {
        private CustomHttpClient _customHttpClient;
        private IUserService _service;

        private readonly IPactBuilderV3 pact;
        readonly string consumerName = "SampleWebApplication";
        readonly string providerName = "ReqRes";

        public UserServiceContractTests()
        {
            ConsumerPact consumerPactConfig = new ConsumerPact(consumerName, providerName);
            this.pact = consumerPactConfig.pact.WithHttpInteractions();
        }
        [Fact]
        public async Task GetUserDetails()
        {
            var id = 1;
            var apiPath = $"/api/users/{id}";

            this.pact
                .UponReceiving("Valid user id to retrieve user details")
                    .Given("Invoking GetUserDetails to get user details with valid id")
                    .WithRequest(HttpMethod.Get, apiPath)
                .WillRespond()
                    .WithStatus(HttpStatusCode.OK)
                    .WithHeader("Content-Type", "application/json; charset=utf-8")
                    .WithJsonBody(Match.Type(new
                    {
                        data = new
                        {
                            id = Match.Equality(id),
                            email = Match.Type("bruce.wayne@reqres.in"),
                            first_name = Match.Type("Bruce"),
                            last_name = Match.Type("wayne")
                        },
                        support = new
                        {
                            text = Match.Type("Bruce wayne is in gotham city"),
                            url = Match.Type("https://gothamcity.com")
                        }
                    }));

            await this.pact.VerifyAsync(async ctx =>
            {
                var baseAddress = ctx.MockServerUri.ToString();
                _customHttpClient = new CustomHttpClient(baseAddress);

                _service = new UserService(_customHttpClient);

                var actualResult = _service.GetUserDetails(id).Result;

                Assert.NotNull(actualResult);
                Assert.Equal(id, actualResult.data.id);
                Assert.Equal("Bruce", actualResult.data.first_name);
                Assert.Equal("wayne", actualResult.data.last_name);
                Assert.Equal("bruce.wayne@reqres.in", actualResult.data.email);
                Assert.Equal("Bruce wayne is in gotham city", actualResult.support.text);
                Assert.Equal("https://gothamcity.com", actualResult.support.url);
            });
        }

        [Fact]
        public async Task CreateUser()
        {
            var apiPath = "/api/users";

            var createUserRequest = new CreateUserRequest()
            {
                name = "Clark Kent",
                job = "Reporter"
            };

            this.pact
                .UponReceiving("Create a user with valid data")
                    .Given("Invoking CreateUser to create user with valid data")
                    .WithRequest(HttpMethod.Post, apiPath)
                    .WithJsonBody(createUserRequest)
                .WillRespond()
                    .WithStatus(HttpStatusCode.Created)
                    .WithHeader("Content-Type", "application/json; charset=utf-8")
                    .WithJsonBody(Match.Type(new
                    {
                        name = Match.Type("Clark Kent"),
                        job = Match.Type("Reporter"),
                        id = Match.Type("503"),
                        createdAt = Match.Type("2023-06-11T08:30:53.624Z")
                    }));

            await this.pact.VerifyAsync(async ctx =>
            {
                var baseAddress = ctx.MockServerUri.ToString();
                _customHttpClient = new CustomHttpClient(baseAddress);

                _service = new UserService(_customHttpClient);

                var actualResult = _service.CreateUser(createUserRequest).Result;

                Assert.NotNull(actualResult);
                Assert.Equal(createUserRequest.name, actualResult.name);
                Assert.Equal(createUserRequest.job, actualResult.job);
                Assert.Equal("503", actualResult.id);
                Assert.Equal("2023-06-11T08:30:53.624Z", actualResult.createdAt);
            });
        }
        [Fact]
        public async Task Login_User()
        {
            var apiPath = "/api/login";

            var LoginUserRequest = new LoginUserRequest()
            {
                email = "eve.holt@reqres.in",
                password = "cityslicka"
            };

            this.pact
                .UponReceiving("Login a user with valid data")
                    .Given("Invoking LoginUser to Login user with valid data")
                    .WithRequest(HttpMethod.Post, apiPath)
                    .WithJsonBody(LoginUserRequest)
                .WillRespond()
                    .WithStatus(HttpStatusCode.OK)
                    .WithHeader("Content-Type", "application/json; charset=utf-8")
                    .WithJsonBody(Match.Type(new
                    {

                        token = Match.Type("QpwL5tke4Pnpja7X4")
                    }));

            await this.pact.VerifyAsync(async ctx =>
            {
                var baseAddress = ctx.MockServerUri.ToString();
                _customHttpClient = new CustomHttpClient(baseAddress);

                _service = new UserService(_customHttpClient);

                var actualResult = _service.LoginUser(LoginUserRequest).Result;

                Assert.Equal("QpwL5tke4Pnpja7X4", actualResult.token);

            });


        }


        }
    }