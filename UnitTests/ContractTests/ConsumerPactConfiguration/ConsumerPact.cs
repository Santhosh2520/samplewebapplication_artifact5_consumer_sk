﻿using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using PactNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.ContractTests.ConsumerPactConfiguration
{
    public class ConsumerPact
    {
        public readonly IPactV3 pact;

        public ConsumerPact(string consumerName, string providerName)
        {
            var config = new PactConfig
            {
                PactDir = "../../../pacts/",

                DefaultJsonSettings = new JsonSerializerSettings
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                }
            };

            pact = Pact.V3(consumerName, providerName, config);

        }

    }
}
