using Moq;

using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using SampleWebApplication_Artifact5_SK.CustomClient;
using SampleWebApplication_Artifact5_SK.Models;
using SampleWebApplication_Artifact5_SK.Services;
using System.Net;
using Xunit;
using System;

namespace UnitTests
{
    public class UserServiceUnitTests
    {
        [Fact]
        public async Task Get_User_Details_With_Valid_Id()
        {
            // Arrange
            var apiPath = "/api/users/1";
            var httpResponseBody = new UserDetails()
            {
                data = new User_Data
                {
                    id = 1,
                    first_name = "RAJA",
                    last_name = "RAM",
                    email = "RAJA.RAM@gmail.com",
                    avatar = "https://gmail.com/images/RAJA_RAM.jpg"
                },
                support = new User_Support()
                {
                    text = "RAJA RAM IS GOOD BOY",
                    url = "https://RAJARAM.com"
                }
            };

            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponse.Content = new ObjectContent<UserDetails>(httpResponseBody, new JsonMediaTypeFormatter());


            var customHttpClientMock = new Mock<ICustomHttpClient>();
            customHttpClientMock.Setup(client => client.GetAsync(apiPath)).ReturnsAsync(httpResponse);

            var userService = new UserService(customHttpClientMock.Object);

            // Act
            var userDetails = await userService.GetUserDetails(1);

            // Assert
            Assert.NotNull(userDetails);
            Assert.Equal(httpResponseBody.data.id, userDetails.data.id);
            Assert.Equal(httpResponseBody.data.email, userDetails.data.email);
            Assert.Equal(httpResponseBody.data.first_name, userDetails.data.first_name);
            Assert.Equal(httpResponseBody.data.last_name, userDetails.data.last_name);
            Assert.Equal(httpResponseBody.data.avatar, userDetails.data.avatar);
            Assert.Equal(httpResponseBody.support.url, userDetails.support.url);
            Assert.Equal(httpResponseBody.support.text, userDetails.support.text);
        }
        [Fact]
        public async Task CreateUser_Valid_Data()
        {
            // Arrange
            var apiUrl = "/api/users";

            var httpRequestBody = new CreateUserRequest()
            {
                name = "RADHA",
                job = "TESTER"
            };

            var httpResponseBody = new CreateUserResponse()
            {
                name = "RADHA",
                job = "TESTER",
                id = "504",
                createdAt = "2022"
            };

            var httpResponse = new HttpResponseMessage(HttpStatusCode.Created);
            httpResponse.Content = new ObjectContent<CreateUserResponse>(httpResponseBody, new JsonMediaTypeFormatter());

            var customHttpClientMock = new Mock<ICustomHttpClient>();


            customHttpClientMock.Setup(client => client.PostJsonAsync(apiUrl, httpRequestBody)).ReturnsAsync(httpResponse);

            var userService = new UserService(customHttpClientMock.Object);

            // Act
            var user = await userService.CreateUser(httpRequestBody);

            // Assert
            Assert.NotNull(user);
            Assert.Equal(httpResponseBody.id, user.id);
            Assert.Equal(httpResponseBody.name, user.name);
            Assert.Equal(httpResponseBody.job, user.job);
            Assert.Equal(httpResponseBody.createdAt, user.createdAt);
        }

        [Fact]
        public async Task Login_User()
        {
            // Arrange
            var apiUrl = "/api/login";

            var httpRequestBody = new LoginUserRequest()
            {
                email = "eve.holt@reqres.in",
                password = "cityslicka"
            };

            var httpResponseBody = new LoginUserResponse()
            {
                token = "QpwL5tke4Pnpja7X4"

            };
            var httpResponse = new HttpResponseMessage(HttpStatusCode.OK);
            httpResponse.Content = new ObjectContent<LoginUserResponse>(httpResponseBody, new JsonMediaTypeFormatter());

            var customHttpClientMock = new Mock<ICustomHttpClient>();


            customHttpClientMock.Setup(client => client.PostJsonAsync(apiUrl, httpRequestBody)).ReturnsAsync(httpResponse);

            var userService = new UserService(customHttpClientMock.Object);

            // Act
            var user = await userService.LoginUser(httpRequestBody);

            // Assert

             Assert.Equal(httpResponseBody.token, user.token);
            Console.WriteLine(user.token);

        }
    }
}